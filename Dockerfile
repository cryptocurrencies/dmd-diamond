# ---- Base Node ---- #
FROM ubuntu:16.04 AS base
RUN apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 8842ce5e && \
    echo "deb http://ppa.launchpad.net/bitcoin/bitcoin/ubuntu xenial main" > /etc/apt/sources.list.d/bitcoin.list
RUN apt-get update && \
    apt-get install -y build-essential libtool autotools-dev autoconf libssl-dev git-core libboost-all-dev libdb4.8-dev libdb4.8++-dev libminiupnpc-dev pkg-config sudo && \
    apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# ---- Build Source ---- #
FROM base as build
RUN git clone https://github.com/DMDcoin/Diamond.git /opt/diamond
RUN cd /opt/diamond/ && \
    ./autogen.sh && \
    ./configure --without-miniupnpc --disable-tests --enable-debug --with-gui=no
RUN cd /opt/diamond/ && \
    make

# ---- Release ---- #
FROM ubuntu:16.04 as release
RUN apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 8842ce5e && \
    echo "deb http://ppa.launchpad.net/bitcoin/bitcoin/ubuntu xenial main" > /etc/apt/sources.list.d/bitcoin.list
RUN apt-get update && \
	apt-get install -y  libboost-all-dev libdb4.8 libdb4.8++ libminiupnpc-dev && \
    apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*
RUN groupadd -r diamond && useradd -r -m -g diamond diamond
RUN mkdir /data
RUN chown diamond:diamond /data
COPY --from=build /opt/diamond/src/diamondd /usr/local/bin/diamondd
COPY --from=build /opt/diamond/src/diamond-cli /usr/local/bin/diamond-cli
USER diamond
VOLUME /data
EXPOSE 9697 9698
CMD ["/usr/local/bin/diamondd", "-datadir=/data", "-conf=/data/diamond.conf", "-server", "-txindex", "-printtoconsole"]